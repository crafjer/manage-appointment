  //Variables
  const mascota = document.querySelector('#mascota');
  const propietario = document.querySelector('#propietario');
  const telefono = document.querySelector('#telefono');
  const fecha = document.querySelector('#fecha');
  const hora = document.querySelector('#hora');
  const sintomas = document.querySelector('#sintomas');
  
  const form = document.querySelector('#nueva-cita');
  const containerCita = document.querySelector('#citas');
  const citas = [];
  data =  {
    mascota: '',
    propietario: '',
    telefono: '',
    fecha: '',
    hora: '',
    sintomas: ''
  };
  let edtting = false;

  class ManageCita {
        constructor() {
            this.citas = [];
        }

        create(cita) {
            this.citas = [...this.citas, cita];
        }

        delete(id) {
           this.citas = this.citas.filter( cita => cita.id !== id);
        }

        update(citaUpdated) {
            this.citas = this.citas.map( (cita) => cita.id === citaUpdated.id ? citaUpdated : cita );
        }
  }

  class UI {

    showAlert(msg, type) {
        const div = document.createElement('div');
        div.className = 'text-center alert col-12';

        if( type === 'error' ) {
            div.classList.add('alert-danger');
        } else {
            div.classList.add('alert-success');
        }

        div.textContent = msg;

        document.querySelector('#contenido').insertBefore(div, document.querySelector('.agregar-cita'));

        setTimeout(function(){
            div.remove();
        }, 3000);
    }

    showCitas(manageCita) {

       this.cleanCitas();

       const {citas} =  manageCita;

       console.log(citas);

       citas.forEach((cita) => {

          const div = this.createElement('div', '', ['cita', 'p-3']);
          const h2 = this.createElement('h2', cita.mascota, ['card-title', 'font-weight-bolder']);
          const p1 = this.createElement('p', '', []);
          p1.innerHTML = `<span class="font-weight-bolder">Propietario: </span>${cita.propietario}`;
          const p2 = this.createElement('p', '', []);
          p2.innerHTML = `<span class="font-weight-bolder">Telefono: </span>${cita.telefono}`;
          const p3 = this.createElement('p', '', []);
          p3.innerHTML = `<span class="font-weight-bolder">Fecha: </span>${cita.fecha}`;
          const p4 = this.createElement('p', '', []);
          p4.innerHTML = `<span class="font-weight-bolder">Hora: </span>${cita.hora}`;
          const p5 = this.createElement('p', '', []);
          p5.innerHTML = `<span class="font-weight-bolder">Sítomas: </span>${cita.sintomas}`;

          const btnEliminar = this.createElement('button',
           'Eliminar <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M6 18L18 6M6 6l12 12" /></svg>', 
           ['btn', 'btn-light', 'mr-2']);
           btnEliminar.onclick = () => eliminarCita(cita.id);

           const btnEdit = this.createElement('button',
           'editar <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" /></svg>', 
           ['btn', 'btn-light', 'mr-2']);
           btnEdit.onclick = () => editarCita(cita);

          div.appendChild(h2);
          div.appendChild(p1);
          div.appendChild(p2);
          div.appendChild(p3);
          div.appendChild(p4);
          div.appendChild(p5);
          div.appendChild(btnEliminar);
          div.appendChild(btnEdit);

          containerCita.appendChild(div);

       });
    }


    cleanCitas() {
        while(containerCita.firstElementChild) {
            containerCita.removeChild(containerCita.firstElementChild);
        }
    }

    createElement(element, text, classes) {
        const newElement = document.createElement(element);
        
        if(text !== '') {
            newElement.innerHTML = text;
        }

        if( classes.length > 0 ) {
            newElement.classList.add(classes[0]);
            newElement.classList.add(classes[1]);
            newElement.classList.add(classes[2]);
        }

        return newElement;
    }

    fiilForm(cita) {
          mascota.value = cita.mascota;
          propietario.value = cita.propietario;
          telefono.value = cita.telefono;
          fecha.value = cita.fecha;
          hora.value = cita.hora;
          sintomas.value = cita.sintomas;
    }
  }

  const manageCita = new ManageCita();
  const ui = new UI();

  //eventlisteners
eventlisteners();

function eventlisteners() {
    mascota.addEventListener('change', getData);
    propietario.addEventListener('change', getData);
    telefono.addEventListener('change', getData);
    fecha.addEventListener('change', getData);
    hora.addEventListener('change', getData);
    sintomas.addEventListener('change', getData);

    form.addEventListener('submit', createCita);
}

function getData(e) {
    data[e.target.name] = e.target.value;
}

function createCita(e) {
    e.preventDefault();
    
    if(data.mascota === '' || data.propietario === '' || data.telefono === '' || data.fecha === '' || 
    data.hora === '' || data.sintomas === '') {
        ui.showAlert('Todos los campos son obligatorios', 'error');
        return;
    }

    if( edtting ) {
        ui.showAlert('Cita se ha editado correctamente.');

        //edit
        manageCita.update({...data});

        form.querySelector('button[type="submit"]').textContent = 'Crear Cita';

        edtting = false;
    } else {
        data.id = Date.now();

        manageCita.create({...data});

        ui.showAlert('Cita se ha agregado correctamente.');
    }

    setData('','','','','','','');

    form.reset();

    ui.showCitas(manageCita);
}

function setData(mascota, propietario, telefono, fecha, hora, sintomas) {
    data.mascota = mascota;
    data.propietario = propietario;
    data.telefono = telefono;
    data.fecha = fecha;
    data.hora = hora;
    data.sintomas = sintomas;
}


function eliminarCita(id) {
    manageCita.delete(id);

    ui.showCitas(manageCita);

    ui.showAlert('Se ha borrado la cita correctamente.');
}

function editarCita(cita) {
    edtting = true;

    ui.fiilForm(cita);

    setData(cita.mascota, cita.propietario, cita.telefono, cita.fecha, cita.hora, cita.sintomas);

    form.querySelector('button[type="submit"]').textContent = 'Guardar Cambios';

   // manageCita.update(id);

    //ui.showCitas(manageCita);

    //ui.showAlert('Se ha borrado la cita correctamente.');
}